import { Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const DeviceWidth = Dimensions.get('window').width;

var { height } = Dimensions.get('window');
 
var box_count = 3;
var box_height = height / box_count;

const styles = {
  cardView: {
   
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scanCardView: {
    width: deviceWidth - 32,
    height: deviceHeight / 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white',
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },


//MODAL
  contenedor: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

  // BASE
  scrollViewStyle: {
    flex: 1,
    backgroundColor: '#fff',
  },
  // Headers
  header: {
    backgroundColor: '#0a0d64',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },

  tinyLogo: {
    width: 210,
    height: 80,

  },
  tinyLogoMontos: {
    width: 10,
    height: 10,
  },
  tinyLogoRedes:{
    width: 30,
    height: 30,
  },
  // Seccion texto
  colorfondoTexto:{
    backgroundColor: '#F8CC23',
  },
  textTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    padding: 10,
    color: '#0a0d64',
  },
  textTitle1: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#0a0d64',
  },
  fecha:{
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#0a0d64',
  },
  textoMensaje: {
    fontSize: 40,
    fontWeight: 'bold',
  },

  //Cuadricula
  borderCuariculas: {
    flexDirection: 'row',
  },

  estiloNumeros:{
    fontSize: 35,
    fontWeight: 'bold',
    color: "#000"
  },

  borderTriple: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },

  montosPremios: {
    flex:1,
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  esitloMontos:{
    fontSize: 20,
    fontWeight: '900',
    color: "#0a0d64"
  },
  esitloMontos2:{
    fontSize: 25,
    fontWeight: '900',
    color: "#0a0d64"
  },
  esitloMontos3:{
    fontSize: 15,
    fontWeight: '900',
    color: "#0a0d64"
  },
  esitloMontos4:{
    fontSize: 10,
    fontWeight: '900',
    color: "#0a0d64"
  },

  // BOTON
  alignBoton: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  buttonTouchable: {
    fontSize: 25,
    backgroundColor: '#0a0d64',
    marginBottom: 15,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius:50,
  },

  btnSalir:{
    fontSize: 25,
    backgroundColor: '#0a0d64',
    marginBottom: 30,
    width: deviceWidth - 100,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius:50,
  },

  buttonTextStyle: {
    color: 'white',
    fontWeight: 'bold',
  },

  //footer
  borderTriple: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom:10,
  },
    footer:{
    backgroundColor: '#0a0d64',
    alignItems: 'center',
  },

};
export default styles;