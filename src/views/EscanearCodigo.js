import React, {Fragment, useState} from 'react';
import {Text, View, Dimensions, Alert, TouchableOpacity,ScrollView,Modal,ImageBackground,Image,Linking} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {Component} from 'react/cjs/react.production.min';
import styles from '../assets/scanStyle';

class EscanearCodigo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qr: '',
      scan: false,
      ScanResult: false,
      result: null,
      visibility: false,
    };
  }

  onSucces = e => {
    const valor = {qr: e.data};
    const datosSorteo = valor.qr;

    // VALIDAR GANADOR
    const cuadriculaValidar = [];
    this.cuadriculaValidar = cuadriculaValidar.push(datosSorteo.slice(-12, -3));
    const arrayCuadriculaValidar = [...cuadriculaValidar[0]];

    const tripleAbierto = [];
    this.tripleAbierto = tripleAbierto.push(datosSorteo.slice(-3));
    const arrayTripleAbierto = [...tripleAbierto[0]];

    const tripleGanador = arrayTripleAbierto[0] + arrayTripleAbierto[1] + arrayTripleAbierto[2];

    const combinacion1 = arrayCuadriculaValidar[2] + arrayCuadriculaValidar[4] + arrayCuadriculaValidar[6];
    const combinacion2 = arrayCuadriculaValidar[2] + arrayCuadriculaValidar[5] + arrayCuadriculaValidar[8];
    const combinacion3 = arrayCuadriculaValidar[1] + arrayCuadriculaValidar[4] + arrayCuadriculaValidar[7];
    const combinacion4 = arrayCuadriculaValidar[0] + arrayCuadriculaValidar[3] + arrayCuadriculaValidar[6];
    const combinacion5 = arrayCuadriculaValidar[0] + arrayCuadriculaValidar[4] + arrayCuadriculaValidar[8];
    const combinacion6 = arrayCuadriculaValidar[0] + arrayCuadriculaValidar[1] + arrayCuadriculaValidar[2];
    const combinacion7 = arrayCuadriculaValidar[3] + arrayCuadriculaValidar[4] + arrayCuadriculaValidar[5];
    const combinacion8 = arrayCuadriculaValidar[6] + arrayCuadriculaValidar[7] + arrayCuadriculaValidar[8];

    this.setState({
      result: e,
      scan: false,
      ScanResult: true,
    });
    const isValor =  valor.qr && valor.qr.length === 23;
    
    this.setState(
      isValor
        ? valor
        : valor.qr && valor.qr.length === 8
        ? this.validacionSerialExterno(valor)
        : this.validacionSerialInterno(),
    );
    if(isValor) this.salirEscaner();
    if(isValor && combinacion1.toString() === tripleGanador.toString()) {
      this.mensajeGanador();
    }else if(isValor && combinacion2.toString() === tripleGanador.toString()){
      this.mensajeGanador2();
    }else if(isValor && combinacion3.toString() === tripleGanador.toString()){
      this.mensajeGanador3();
    }else if(isValor && combinacion4.toString() === tripleGanador.toString()){
      this.mensajeGanador4();
    }else if(isValor && combinacion5.toString() === tripleGanador.toString()){
      this.mensajeGanador5();
    }else if(isValor && combinacion6.toString() === tripleGanador.toString()){
      this.mensajeGanador6();
    }else if(isValor && combinacion7.toString() === tripleGanador.toString()){
      this.mensajeGanador7();
    }else if(isValor && combinacion8.toString() === tripleGanador.toString()){
      this.mensajeGanador8();
    }
    else if(isValor){
      this.mensajeNoGanador();
    }

  };

  activeQR = () => {
    this.setState({
      scan: true,
    });
    this.setModalVisibility(true);
  };

  scanAgain = () => {
    this.setState({
      scan: true,
      ScanResult: false,
      qr: '',
    });
    this.setModalVisibility(true);
  };

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  mensajeGanador = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 5$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador2 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 15$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador3 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 50$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador4 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 10$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador5 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 6.000$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador6 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 25$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador7 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 100$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeGanador8 = () => {
    Alert.alert(
      '¡Felicidades!',
      'Eres ganador de un premio de 2.500$',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };
  mensajeNoGanador = () => {
    Alert.alert(
      '¡Lo sentimos!',
      'No Eres ganador',
      [{text: 'OK', onPress: () => console.log("bien")}],
    );
  };

  validacionSerialExterno = valorExterno => {
    Alert.alert(
      'Triple Gordo informa',
      'Por favor contacte a su comprador comercial para validar premio. Su código de validación es:' +
        ' ' +
        valorExterno.qr,
      [{text: 'OK', onPress: () => this.setModalVisibility(!this.state.visibility)}],
    );
  };

  validacionSerialInterno = () => {
    Alert.alert(
      'Triple Gordo informa',
      'No se pudo leer correctamente el código QR por favor intente nuevamente',
      [{text: 'OK', onPress: () => this.setModalVisibility(!this.state.visibility)}],
    );
  };

  salirEscaner = () =>{
    this.setState({scan: false});
    this.setModalVisibility(!this.state.visibility);
  }

  render() {
    const {scan, ScanResult} = this.state;
    const DeviceWidth = Dimensions.get('window').width;
    const tiraDatos = this.state.qr;

    const image = require('../assets/fondo_recortado.png');

    // CUADRICULA
    const cuadricula = [];
    this.cuadricula = cuadricula.push(tiraDatos.slice(-12, -3));
    const arrayCuadricula = [...cuadricula[0]];

    // Triple
    const triple = [];
    this.triple = triple.push(tiraDatos.slice(-3));
    const arrayTriple = [...triple[0]];
    // console.log("triple",triple)

    // NUMERO SORTEO
    const numSorteo = [];
    this.numSorteo = numSorteo.push(tiraDatos.slice(0, 3));

    // DIA
    const diaSorteo = [];
    this.diaSorteo = diaSorteo.push(tiraDatos.slice(3, 5));
    // MES
    const mesSorteo = [];
    this.mesSorteo = mesSorteo.push(tiraDatos.slice(5, 7));
    // AÑO
    const annoSorteo = [];
    this.annoSorteo = annoSorteo.push(tiraDatos.slice(7, 11));

    return (
      <ScrollView style={styles.scrollViewStyle}>
        <View>
          <Modal
            animationType={'slide'}
            transparent={false}
            visible={this.state.visibility}>
            <View style={(styles.modalContainer, styles.cardView)}>
              <View>
                {/* SCANER */}
                {scan && (
                  <QRCodeScanner
                    reactivate={true}
                    onRead={this.onSucces}
                    showMarker={true}
                    ref={node => {
                      this.scanner = node;
                    }}
                  />
                )}
                <View style={styles.alignBoton}>
                  <TouchableOpacity
                    onPress={this.salirEscaner}
                    style={styles.btnSalir}>
                    <Text style={styles.buttonTextStyle}>Salir</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
        <Fragment>
          <View>
            {/* HEADER */}
            <View style={styles.header}>
              <Image
                style={styles.tinyLogo}
                source={require('../assets/logoTG.png')}
              />
            </View>
            {/* TEXTO */}
            <View style={styles.colorfondoTexto}>
              <Text style={styles.textTitle}>
                VERIFICACIÓN DE PREMIO INSTANTANEO
              </Text>
            </View>
            {/* IMG BACKGROUD */}
            <ImageBackground
              source={image}
              resizeMode="cover"
              style={styles.image}>
              {/* CUADRICULA */}
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 20,
                }}>
                {/* numero de sorteo y fecha */}
                {tiraDatos.length > 0 ? (
                  <>
                    <Text style={styles.textTitle1}>Sorteo N° {numSorteo}</Text>
                    <Text style={styles.fecha}>
                      {diaSorteo}/{mesSorteo}/{annoSorteo}
                    </Text>
                  </>
                ) : (
                  <></>
                )}

                <View style={styles.borderCuariculas}>
                  {/* INICIO COLUMNA 0 */}
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos2}>6.000</Text>
                      <Text style={styles.esitloMontos3}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/6000.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 25</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaDerecha.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 100</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaDerecha.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 2.500</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaDerecha.png')}
                      />
                    </View>
                  </View>
                  {/* INICIO COLUMNA 1 */}
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 10</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaAbajo.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderTopWidth: 4,
                        borderLeftWidth: 4,
                        borderRightWidth: 1,
                        borderBottomWidth: 1,
                        borderTopLeftRadius:20,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[0]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderLeftWidth: 4,
                        borderTopWidth: 1,
                        borderRightWidth: 1,
                        borderBottomWidth: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[3]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderBottomWidth: 4,
                        borderLeftWidth: 4,
                        borderTopWidth: 1,
                        borderRightWidth: 1,
                        borderBottomLeftRadius:20,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[6]}
                      </Text>
                    </View>
                  </View>
                  {/* INICIO COLUMNA 2 */}
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 50</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaAbajo.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderTopWidth: 4,
                        borderBottomWidth: 1,
                        borderLeftWidth: 1,
                        borderRightWidth: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[1]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderTopWidth: 1,
                        borderBottomWidth: 1,
                        borderLeftWidth: 1,
                        borderRightWidth: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[4]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderBottomWidth: 4,
                        borderTopWidth: 1,
                        borderLeftWidth: 1,
                        borderRightWidth: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[7]}
                      </Text>
                    </View>
                  </View>
                  {/* INICIO COLUMNA 3 */}
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>$ 15</Text>
                      <Text style={styles.esitloMontos4}>DÓLARES</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaAbajo.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderTopWidth: 4,
                        borderRightWidth: 4,
                        borderBottomWidth: 1,
                        borderLeftWidth: 1,
                        borderTopRightRadius:20,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[2]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderTopWidth: 1,
                        borderRightWidth: 4,
                        borderBottomWidth: 1,
                        borderLeftWidth: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[5]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        backgroundColor: 'transparent',
                        borderColor: '#0a0d64',
                        borderBottomWidth: 4,
                        borderRightWidth: 4,
                        borderTopWidth: 1,
                        borderLeftWidth: 1,
                        borderBottomRightRadius:20,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>
                        {arrayCuadricula[8]}
                      </Text>
                    </View>
                  </View>
                  {/* INICIO COLUMNA 4 */}
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.2,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.esitloMontos3}>Reintregro</Text>
                      <Image
                        style={styles.tinyLogoMontos}
                        source={require('../assets/flechaIzquierda.png')}
                      />
                    </View>
                    <View
                      style={{
                        width: DeviceWidth * 0.2,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                      }}></View>
                    <View
                      style={{
                        width: DeviceWidth * 0.2,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                      }}></View>
                    <View
                      style={{
                        width: DeviceWidth * 0.2,
                        height: DeviceWidth * 0.19,
                        backgroundColor: 'transparent',
                      }}></View>
                  </View>
                </View>
              </View>
              {/* TRIPLE */}
              <View style={styles.borderTriple}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.15,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        backgroundColor: 'transparent',
                        borderColor: 'red',
                        borderBottomWidth: 4,
                        borderTopWidth: 4,
                        borderLeftWidth: 4,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>{arrayTriple[0]}</Text>
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.15,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        backgroundColor: 'transparent',
                        borderColor: 'red',
                        borderBottomWidth: 4,
                        borderTopWidth: 4,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>{arrayTriple[1]}</Text>
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.15,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        backgroundColor: 'transparent',
                        borderColor: 'red',
                        borderBottomWidth: 4,
                        borderRightWidth: 4,
                        borderTopWidth: 4,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.estiloNumeros}>{arrayTriple[2]}</Text>
                    </View>
                  </View>
                </View>
              </View>
              {/* BOTÓN VALIDAR */}
              {!scan && !ScanResult && (
                <View style={styles.alignBoton}>
                  <TouchableOpacity
                    onPress={this.activeQR}
                    style={styles.buttonTouchable}>
                    <Text style={styles.buttonTextStyle}>VALIDAR</Text>
                  </TouchableOpacity>
                </View>
              )}
              {ScanResult && (
                <Fragment>
                  <View style={styles.alignBoton}>
                    <TouchableOpacity
                      onPress={this.scanAgain}
                      style={styles.buttonTouchable}>
                      <Text style={styles.buttonTextStyle}>
                        VALIDAR DE NUEVO!
                      </Text>
                    </TouchableOpacity>
                  </View>
                </Fragment>
              )}
            </ImageBackground>
            {/* FOOTER */}
            <View style={styles.footer}>
              <View style={styles.borderTriple}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL('https://twitter.com/triplegordo_ofi')
                        }>
                        <Image
                          style={styles.tinyLogoRedes}
                          source={require('../assets/twitter.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL('https://www.facebook.com/TripleGordoV/')
                        }>
                        <Image
                          style={styles.tinyLogoRedes}
                          source={require('../assets/facebook.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        width: DeviceWidth * 0.19,
                        height: DeviceWidth * 0.15,
                        marginBottom: 1,
                        flex: 1,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL('https://www.instagram.com/triplegordo_oficial/')
                        }>
                        <Image
                          style={styles.tinyLogoRedes}
                          source={require('../assets/instagram.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Fragment>
      </ScrollView>
    );
  }
}

export default EscanearCodigo;
