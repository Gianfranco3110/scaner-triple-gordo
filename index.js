/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import EscanearCodigo from './src/views/EscanearCodigo';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => EscanearCodigo);
